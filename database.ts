import * as ottoman from "ottoman";
import * as couchbase from 'couchbase';
import { IDataConfiguration } from "./config";
import { IExample, ExampleSchema } from "./src/models/example";

export interface IDatabase {
  exampleModel: ottoman.model<IExample>;
}

export function init(config: IDataConfiguration): IDatabase {

  (<any>ottoman).Promise = Promise;
  const cluster = new couchbase.Cluster(config.connectionString);
  ottoman.openBucket =  cluster.openBucket('default');
  //const connection = Ottoman.createConnection(config.connectionString);

  /*connection.on('error', () => {
    console.log('Unable to connect to database: ' + config.connectionString);
  });

  connection.once('open', () => {
    console.log('Connected to database: ' + config.connectionString);
  });*/

  const exampleModel = ottoman.model<IExample> ('Example', ExampleSchema);

  return {
      exampleModel: exampleModel,
  };
}