import * as Ottoman from "ottoman";
import * as ErrorUtil from "../commons/util/error";

export abstract class DatabaseService {
  private model: Ottoman.model;
  
  constructor (model: Ottoman.model) {
    this.model = model;
  }

  public async findAll() : Promise<Ottoman.document> {
    try {
      return await this.model.find({});
    } catch (error) {
      console.log('[DataBaseService - findAll]: ', error);
      throw ErrorUtil.generateErrorService(error); 
    }
  };

  public async add(document: Ottoman.document) : Promise<Ottoman.document> {
    try {
      return await this.model.create(document);
    } catch (error) {
      console.log('[DataBaseService - add]: ', error);
      throw ErrorUtil.generateErrorService(error);
    }    
  }

  public async findById(id: string) : Promise<Ottoman.document> {
    try {
      return await this.model.findOne({_id: id});
    } catch (error) {
      console.log('[DataBaseService - findById]: ', error);
      throw ErrorUtil.generateErrorService(error);
    }
  }
  
  public update (id: string, document: Ottoman.document) : Promise<Ottoman.document> {
    try {
      return this.model.findByIdAndUpdate({_id: id}, {$set: document}, {new: true});
    } catch (error) {
      console.log('[DataBaseService - update]: ', error);
      throw ErrorUtil.generateErrorService(error); 
    }
  }

  public remove (id: string) {
    try {
      return this.model.remove({ _id: id});
    } catch (error) {
      console.log('[DataBaseService - remove]: ', error);
      throw ErrorUtil.generateErrorService(error);
    }
  }
}