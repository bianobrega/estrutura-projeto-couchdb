import { IDatabase } from "../../database";
import { DatabaseService }  from './database';

export default class ExampleService extends DatabaseService  {
  constructor (dataBase: IDatabase) {
    super(dataBase.exampleModel);
  }
}