import * as Express from 'express';
import ExampleRoute from './routes/example';
import { IServerConfigurations } from '../config';
import { IDatabase }  from '../database';

export function init(server, configs: IServerConfigurations, database: IDatabase) {
  ExampleRoute(server, configs, database);
}
