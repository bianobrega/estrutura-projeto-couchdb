import * as Ottoman from "ottoman";

export interface IExample extends Ottoman.Document {
  name: string;
  description: string;
  creationDate: Date;
}
export const ExampleSchema = {
  name: { type: String, required: [true, 'mensagem'] },
  description: String,
  creationDate: { type: Date, default: Date.now},
};
