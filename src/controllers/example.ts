import * as Express from 'express';
import { IDatabase } from "../../database";
import { IServerConfigurations } from "../../config";
import { HTTP_CODES } from '../commons/constants/constants';

import * as ErrorUtil from '../commons/util/error';
import ExampleService from '../services/example';

export default class ExampleController {
  private database: IDatabase;
  private configs: IServerConfigurations;
  private exampleService: ExampleService;

  constructor(configs: IServerConfigurations, database: IDatabase) {
    this.database = database;
    this.configs = configs;
    this.exampleService = new ExampleService(database);
  }

  public async findAll(req: Express.Request, res: Express.Response) {
    console.log(this);
    const teste = new ExampleService(this.database);
    try {
      const result = await teste.findAll();

      if (typeof result !== 'undefined' && result.length > 0) {
        res.send(result);
      } else {
        res.sendStatus(HTTP_CODES.noContent);
      }

    } catch (error) {
      res.status(ErrorUtil.generateCodeHttp(error)).send(error.message);
    } 
  }

  public async add(req: Express.Request, res: Express.Response) {
    try {
      const result = await this.exampleService.add(req.body);
      res.status(HTTP_CODES.created).send(result);

    } catch (error) {
      res.status(ErrorUtil.generateCodeHttp(error)).send(error.message);
    }
  }

  public async findById(req: Express.Request, res: Express.Response) {
    try {
      const id = req.params.id;
      const result = await this.exampleService.findById(id);

      if (result) {
        res.send(result);
      } else {
				res.sendStatus(HTTP_CODES.noContent);
      }
    } catch (error) {
      res.status(ErrorUtil.generateCodeHttp(error)).send(error.message);
    }
  }
    
  public async update(req: Express.Request, res: Express.Response) {
    try {
      const result = await this.exampleService.update(req.params.id, req.body);
      res.send(result);
      
    } catch (error) {
      res.status(ErrorUtil.generateCodeHttp(error)).send(error.message);
    }
  }
}
