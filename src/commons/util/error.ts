import { ERRORS, MONGO_ERRORS, HTTP_CODES} from '../../commons/constants/constants';
import { IError, IErrorMessage } from "../../models/error";

export function generateErrorService (param) : IError {
	let error: IError;

	if (param.code === MONGO_ERRORS.uniqueError) {
		let errorMessage: IErrorMessage;
    var regex = /index\:\ (?:.*\.)?\$?(?:([_a-z0-9]*)(?:_\d*)|([_a-z0-9]*))\s*dup key/i,      
    match =  param.message.match(regex),  
    indexName = match[1] || match[2]; 

		errorMessage = {
			message: indexName + ' já utilizado.'
		};
		
		error.type = ERRORS.conflict;
		error.errors.push(errorMessage);

	} else if (param.name === MONGO_ERRORS.validationErrorName) {
		let errorMessages: IErrorMessage[];
		errorMessages = Object.keys(param.errors).map((errorField) => {
			return {message: param.errors[errorField].message};
		});

		error.type = ERRORS.badRequest;
		error.errors = errorMessages;
		
	} else {
		let errorMessage: IErrorMessage = {
			message: 'Ocorreu um erro inesperado. Tente novamente mais tarde.'
		};
		error.type = ERRORS.internalServerError;
		error.errors.push(errorMessage);
	}

	return error;
}

export function generateCodeHttp (error) : number {
	let httpCode: number;

	switch (error.type) {
		case ERRORS.badRequest: 
			httpCode = HTTP_CODES.badRequest;
			break;
    case ERRORS.conflict: 
			httpCode = HTTP_CODES.conflict;
			break;
    case ERRORS.internalServerError:
			httpCode = HTTP_CODES.internalServerError;
			break;
		default: 
			httpCode = HTTP_CODES.internalServerError;
			break;
	}

	return httpCode;
} 